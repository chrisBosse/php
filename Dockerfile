FROM php:5.6.40-apache-jessie

# # Get app source
RUN curl https://gitlab.com/chrisBosse/php/raw/barcode-test/test.php > index.php \
&& touch test.bmp \
&& chmod o+w test.bmp