<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en" dir="ltr">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<style type="text/css">
	body  {font-family: Verdanda,Arial,sans-serif;
	font-weight: bold;
	}
	</style>
<title>Tracking Label Test</title>
</head>

<body font="verdana">
Welcome to C-BRUSH, the online Barcode relabel utility.<p>
In the future, this utility will calculate obscured digits from valid barcodes and display a new printable barcode matching the tracking number that was found.<p>
<?php
//if (isset($_POST['action']) && filter_input(INPUT_POST, 'action', FILTER_SANITIZE_STRING) == 'submitted') {
if (isset($_POST['action']) && $_POST['action'] == 'submitted') {
    //$test = filter_input(INPUT_POST, 'bc', FILTER_SANITIZE_STRING);
    $test = $_POST['bc'];
    echo '<pre>';
    //print_r($_POST);
    echo '<a href="'. $_SERVER['PHP_SELF'] .'">Please try again</a>';
    echo '</pre>';
} else {
	$test = '11020987654312345672';
?>

<form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
    Enter Barcode: <input type="text" name="bc" /><br />
    <input type="hidden" name="action" value="submitted" />
    <input type="submit" name="submit" value="submit me!" />
</form>
<?php
}
?>


<?php
// PHP script

//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
// Validate Tracking number
// given user inputed tracking number minus leading "96"
// 

//[96] 11019012345612345671
//$test = '11019012345612345671';
//$test = '11020987654312345672';
// Force Start with "96"
//  Only accept remaining digits.
//  prevent printing free labels.

// Correct length?
// Just incase, remove any leading 96.  There is no 96
//  following the starting 96 in a currently valid FedEx tracking ID label
if ((substr($test, 0, 1) == "9") and (substr($test, 1, 1) == "6"))
	$test = substr($test, 2);
if (strlen($test) != 20) echo 'Length_Error';

// Check all numbers
// Doesn't work yet
//for ($i = 0; $i < strlen($test); $i++) {
//	echo " " . $i . "-" . substr($test, $i, 1);
//	if ((substr($test, $i, 1) >= 0) && (substr($test, $i, 1) <= 9)) {
//		echo ".";
//	} else { echo "x" ;}
//}

//Append 96
$test = '96' . $test;
//echo "</br>" . $test . "</br>";

Function Calculate_Checksum ( $arg_1 )
{
	// Expect a 96 barcode with at least the
	//   Shipper ID, Pkg ID, something in checksum

	// ADD EVEN NUMBERS, starting from right, base 1
	$temp1 = 0;
	for ($i = 2; $i <= 14; $i += 2) {
		$temp1 += substr($arg_1, (strlen($arg_1) - $i), 1);
	}

	// MULTIPLY BY 3
	$temp1 = $temp1 * 3;

	// ADD ODD NUMBERS, starting from right, base 1, skip checksum
	$temp2 = 0;
	for ($i = 3; $i <= 15; $i += 2) {
		$temp2 += substr($arg_1, (strlen($arg_1) - $i), 1);
	}

	// ADD RESULTS 1 AND RESULTS 2
	$temp3 = $temp1 + $temp2;

	// FIND SMALLEST NUMBER ADDED TO SUM TO MAKE A MULTIPLE OF 10
	$check = 10 - ($temp3 % 10);
	return $check;
}

// Compare checksum
if (substr($test, (strlen($test) - 1), 1) != Calculate_Checksum($test)) echo "CHECKSUM_ERROR";


//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
// BarCode Table
// Code 128
// 
//echo "Test before table";
$EAN_Table = array(
	"00" => "11011001100",
	"01" => "11001101100",
	"02" => "11001100110",
	"03" => "10010011000",
	"04" => "10010001100",
	"05" => "10001001100",
	"06" => "10011001000",
	"07" => "10011000100",
	"08" => "10001100100",
	"09" => "11001001000",
	"10" => "11001000100",
	"11" => "11000100100",
	"12" => "10110011100",
	"13" => "10011011100",
	"14" => "10011001110",
	"15" => "10111001100",
	"16" => "10011101100",
	"17" => "10011100110",
	"18" => "11001110010",
	"19" => "11001011100",
	"20" => "11001001110",
	"21" => "11011100100",
	"22" => "11001110100",
	"23" => "11101101110",
	"24" => "11101001100",
	"25" => "11100101100",
	"26" => "11100100110",
	"27" => "11101100100",
	"28" => "11100110100",
	"29" => "11100110010",
	"30" => "11011011000",
	"31" => "11011000110",
	"32" => "11000110110",
	"33" => "10100011000",
	"34" => "10001011000",
	"35" => "10001000110",
	"36" => "10110001000",
	"37" => "10001101000",
	"38" => "10001100010",
	"39" => "11010001000",
	"40" => "11000101000",
	"41" => "11000100010",
	"42" => "10110111000",
	"43" => "10110001110",
	"44" => "10001101110",
	"45" => "10111011000",
	"46" => "10111000110",
	"47" => "10001110110",
	"48" => "11101110110",
	"49" => "11010001110",
	"50" => "11000101110",
	"51" => "11011101000",
	"52" => "11011100010",
	"53" => "11011101110",
	"54" => "11101011000",
	"55" => "11101000110",
	"56" => "11100010110",
	"57" => "11101101000",
	"58" => "11101100010",
	"59" => "11100011010",
	"60" => "11101111010",
	"61" => "11001000010",
	"62" => "11110001010",
	"63" => "10100110000",
	"64" => "10100001100",
	"65" => "10010110000",
	"66" => "10010000110",
	"67" => "10000101100",
	"68" => "10000100110",
	"69" => "10110010000",
	"70" => "10110000100",
	"71" => "10011010000",
	"72" => "10011000010",
	"73" => "10000110100",
	"74" => "10000110010",
	"75" => "11000010010",
	"76" => "11001010000",
	"77" => "11110111010",
	"78" => "11000010100",
	"79" => "10001111010",
	"80" => "10100111100",
	"81" => "10010111100",
	"82" => "10010011110",
	"83" => "10111100100",
	"84" => "10011110100",
	"85" => "10011110010",
	"86" => "11110100100",
	"87" => "11110010100",
	"88" => "11110010010",
	"89" => "11011011110",
	"90" => "11011110110",
	"91" => "11110110110",
	"92" => "10101111000",
	"93" => "10100011110",
	"94" => "10001011110",
	"95" => "10111101000",
	"96" => "10111100010",
	"97" => "11110101000",
	"98" => "11110100010",
	"99" => "10111011110",
	"100" => "10111101110",
	"101" => "11101011110",
	"102" => "11110101110",
	"103" => "11010000100",
	"104" => "11010010000",
	"105" => "11010011100",
	"129" => "1100011101011",
);


function Make_Binary($arg_3, $table_src) {
	//---------------------------------------------------------------------------
	//---------------------------------------------------------------------------
	// Make Binary
	// given valid tracking number
	// 
	$temp = 105 + 102;
	$bin = "00000000000" . $table_src[105] . $table_src[102];
	
	for ($i = 0; $i < strlen($arg_3); $i += 2) {
		//echo substr($test, $i, 2) . " - " . (($i / 2) + 2) . " - " . substr($test, $i, 2) * (($i / 2) + 2) . "<br>\n";
		$temp += substr($arg_3, $i, 2) * (($i / 2) + 2);
		$bin .= $table_src[substr($arg_3, $i, 2)];
	}
	$bin .= $table_src[($temp % 103)] . $table_src[129] . "00000000000";
	return $bin;
}


Function Make_Image($arg_2)
{
	//---------------------------------------------------------------------------
	//---------------------------------------------------------------------------
	// Make Image
	// 
	// 
	
	$bar = 4;
	//http://www.martinreddy.net/gfx/2d/BMP.txt
	//BITMAPFILEHEADER
	//B,M, file size, reserved, reserved, ?????
	$BMP  = pack("CCVvvV", 66,77,((strlen($arg_2)+2)*$bar + 8)*191/8 + 62,0,0,62);
	//bitmap info size, width, height, planes, count, compression, sizeimage, 
	// XPelsPerMeter, YPelPerMeter, Color used, Color Important.
	$BMP .= pack("VVVvvVVVVVV", 40, strlen($arg_2)*$bar, 191, 1, 1, 0, ((strlen($arg_2)+2)*$bar + 8)*191/8, 0, 0, 0, 0);
	// slight correction
	$BMP .= pack("V", 0);
	//rgbQuad
	$BMP .= pack("CCCC", 255, 255, 255, 0);
	
	for ($j = 0; $j < 1; $j++) {
		for ($i = 0; $i < strlen($arg_2); $i += 2) {
			$BMP .= chr(255);
		}
		$BMP .= chr(00);
	}
	
	for ($j = 0; $j < (191 -1); $j++) {
		for ($i = 0; $i < strlen($arg_2); $i += 2) {
			$BMP .= chr( 255 - bindec(
				     substr($arg_2, $i, 1) .
				     substr($arg_2, $i, 1) .
				     substr($arg_2, $i, 1) .
				     substr($arg_2, $i, 1) .
				     substr($arg_2, $i + 1, 1) .
				     substr($arg_2, $i + 1, 1) .
				     substr($arg_2, $i + 1, 1) .
				     substr($arg_2, $i + 1, 1) ) );
		}
		$BMP .= chr(00);
	}
	return $BMP;
}

function Save_File($filename, $somecontent) {
	// Let's make sure the file exists and is writable first.
	if (is_writable($filename)) {
	
	    // In our example we're opening $filename in append mode.
	    // The file pointer is at the bottom of the file hence
	    // that's where $somecontent will go when we fwrite() it.
	    if (!$handle = fopen($filename, 'wb')) {
		 echo "Cannot open file ($filename)";
		 exit;
	    }
	
	    // Write $somecontent to our opened file.
	    if (fwrite($handle, $somecontent) === FALSE) {
		echo "Cannot write to file ($filename)";
		exit;
	    }
	
	    //echo "Success, wrote to file ($filename)";
	
	    fclose($handle);
	
	} else {
	    echo "The file $filename is not writable";
	}
}

$temp = Make_Binary($test, $EAN_Table);
$temp2 = Make_Image($temp);
$temp3 = Save_File('test.bmp', $temp2);


?>

<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
C-BRUSH&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CAPE 0026&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;RELABEL
<br>
<img src="test.bmp" width="364" height="191"/>
<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<?php
echo "(" . substr($test, 0,7) . ")&nbsp;&nbsp;&nbsp;&nbsp;" . substr($test, 7,7) . "&nbsp;&nbsp;&nbsp;&nbsp;" . substr($test, 14); ?>
</body>
</html>
