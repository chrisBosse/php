#!/bin/bash

image_name=barcode-image
container_name=barcode-container

docker build -t "$image_name" .
docker run -dit --rm --name "$container_name" "$image_name"

# Helpful Printout
echo
echo 'Connect to the web server with a browser pointed to'
echo
IPS=$( docker inspect --format '{{range .NetworkSettings.Networks}}{{.IPAddress}} {{end}}' "$container_name" )
echo "http://$IPS"
